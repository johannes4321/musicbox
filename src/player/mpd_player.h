#pragma once

#include <exception>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include <absl/strings/string_view.h>

struct mpd_connection;
namespace boost::program_options {
  class options_description;
  class variables_map;
}

namespace musicbox::player {
class mpd_error : public std::runtime_error {
public:
    explicit mpd_error(const std::string& what_arg) : runtime_error{what_arg} {
    }
    explicit mpd_error(const char* what_arg) : runtime_error{what_arg} {};
};

class mpd_player {
    std::unique_ptr<mpd_connection, void(*)(mpd_connection*)> mpd;

   public:
    class po {
        std::string host;
        unsigned port;
        std::string password;

       public:
	constexpr static char name[] = "MPD Player";
        boost::program_options::options_description commandline_options();
        mpd_player create_instance() const;
    };

    explicit mpd_player(const std::string& host, unsigned port, const std::string& password);

    std::pair<std::string, std::string> get_current_id_and_title() const noexcept;
    bool play(absl::string_view songid) const noexcept;

    std::vector<std::pair<std::string, std::string>> get_playlists() const;
    bool play_list(absl::string_view listid) const noexcept { return false; }
};
}  // namespace musicbox::player
