#include "mpd_player.h"

#include "mpd/client.h"
#include <boost/program_options.hpp>

namespace musicbox::player {

boost::program_options::options_description mpd_player::po::commandline_options() {
    namespace po = boost::program_options;
    boost::program_options::options_description descr{"MPD Options"};
    descr.add_options()
	    ("mpd-host", po::value<std::string>(&host)->default_value("localhost"), "MPD host")
	    ("mpd-port", po::value<unsigned>(&port)->default_value(6600), "MPD port")
	    ("mpd-password", po::value<std::string>(&password)->default_value(""), "MPD password");

    return descr;
}
mpd_player mpd_player::po::create_instance() const {
    return mpd_player{host, port, password};
}

mpd_player::mpd_player(const std::string& host, unsigned port, const std::string& password)
    : mpd{mpd_connection_new(host.c_str(), port, 1000), &mpd_connection_free} {
    auto m = mpd.get();
    if (mpd_connection_get_error(m) != MPD_ERROR_SUCCESS) {
        throw mpd_error(mpd_connection_get_error_message(m));
    }

    if (!mpd_run_password(m, password.c_str())) {
        throw mpd_error("Failed to set password");
    }
}

std::pair<std::string, std::string> mpd_player::get_current_id_and_title() const noexcept {
    auto m = mpd.get();

    auto song = std::unique_ptr<mpd_song, decltype(&mpd_song_free)>{mpd_run_current_song(m),
                                                                    &mpd_song_free};
    if (!song) {
        return std::make_pair(std::string{}, std::string{});
    }

    auto s = song.get();
    return std::make_pair(mpd_song_get_uri(s), std::string{} +
                                                   mpd_song_get_tag(s, MPD_TAG_ARTIST, 0) + " - " +
                                                   mpd_song_get_tag(s, MPD_TAG_TITLE, 0));
}

bool mpd_player::play(absl::string_view songid) const noexcept {
    auto m = mpd.get();
    auto id = mpd_run_add_id(m, songid.data());
    if (id == -1) {
        return false;
    }
    return mpd_run_play_id(m, id);
}

std::vector<std::pair<std::string, std::string>> mpd_player::get_playlists() const {
    auto m = mpd.get();
    std::vector<std::pair<std::string, std::string>> retval{};

    auto pl_loaded = mpd_send_list_playlists(m);
    if (!pl_loaded) {
        throw mpd_error("Failed to load playlists");
    }

    while (auto pl = mpd_recv_playlist(m)) {
        retval.emplace_back(std::pair<std::string, std::string>{mpd_playlist_get_path(pl),
                                                                mpd_playlist_get_path(pl)});
        mpd_playlist_free(pl);
    }

    return retval;
}

}  // namespace musicbox::player
