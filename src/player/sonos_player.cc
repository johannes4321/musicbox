#include "sonos_player.h"

#include <noson/sonossystem.h>
#include <noson/contentdirectory.h>
#include <noson/avtransport.h>
#include <noson/musicservices.h>
#include <noson/smapi.h>
#include <noson/didlparser.h>

#include "../../lib/noson/noson/src/private/debug.h"

#include <iostream>

namespace musicbox::player {

namespace {
void handleEventCB(void* handle) {
    fprintf(stderr, "#########################\n");
    fprintf(stderr, "### Handling event CB ###\n");
    fprintf(stderr, "#########################\n");
}
}  // namespace

boost::program_options::options_description sonos_player::commandline_options() {
    namespace po = boost::program_options;
    boost::program_options::options_description descr{"SONOS Options"};
    descr.add_options()("sonos-group", po::value<std::string>()->default_value("some_group"),
                        "sonos group");

    return descr;
}
sonos_player::config sonos_player::create_config(const boost::program_options::variables_map& vm) {
    return {};
}

sonos_player::sonos_player(const config &cfg) {
    // SONOS::DBGLevel(4);
    std::cout << "Here i am\n";
    SONOS::System sonos(0, nullptr); // */ handleEventCB);
    if (sonos.Discover()) {
        auto zones = sonos.GetZoneList();
        for (auto&& zone : zones) {
            std::cout << "Zone: " << zone.first << "\n";
            SONOS::Player player{zone.second->GetCoordinator()};
            std::cout << " " << player.GetHost() << "\n";
            if (zone.second->GetZoneName() == "Oben") {
                SONOS::ElementList list;
                player.GetPositionInfo(list);
		if (player.PlayStream("x-sonos-spotify:spotify%3atrack%3a0TAIqZLTeMuG8klHtPVVrL?sid=9&flags=8224&sn=1", "")) {
std::cout << "Playing\n";
		} else {
			std::cout << "Not playing\n";
		}
                auto uri = list.FindKey("TrackURI");
                if (uri != list.end()) {
                    std::cout << "URI: " << *uri->get() << "\n";
                }
                for (auto&& ele : list) {
                    std::cout << "  " << ele->XML() << "\n";  // GetKey() << "\n";
                }
            }
        }
        //  auto zone = zones.find("Oben")->second;
        //  auto player = SONOS::Player{zone->GetCoordinator()};
        // std::cout << "YAY " << player.GetHost() << "\n";
    } else {
        std::cout << "NAY\n";
    }
}
}  // namespace musicbox::player
