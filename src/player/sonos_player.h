#pragma once

#include <boost/program_options.hpp>

#include <string>

namespace musicbox::player {
class sonos_player {
   public:
    struct config {
        std::string group;
    };
    static boost::program_options::options_description commandline_options();
    static config create_config(const boost::program_options::variables_map &vm);

    explicit sonos_player(const config &cfg);
    std::pair<std::string, std::string> get_current_id_and_title() noexcept {
        return std::make_pair(std::string{}, std::string{"huch"});
    }
    bool play(const std::string &songid) noexcept {
        return false;
    }

    std::vector<std::pair<std::string, std::string>> get_playlists() const {
        return {};
    }
};
}  // namespace musicbox::player
