
#define BOOST_ERROR_CODE_HEADER_ONLY
#include <boost/asio/io_context.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/posix/stream_descriptor.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/bind.hpp>
#include <boost/program_options.hpp>
#include <csignal>
#include <iostream>
#include <thread>

#include "musicbox_config.h"

#include "cards.h"
#include "cli.h"
#include "http.h"
#include "main_controller.h"
#include "player/player.h"
#include "storage/sqlite_storage.h"

/* We could change this, but we need to be careful about locking
 * as this is a system where not much is happening being single-threaded 
 * should work fine */
static const int thread_count = 1;
static boost::asio::io_context ioc{thread_count}; // NOLINT(cert-err58-cpp)
static std::vector<std::thread> threads;

namespace po = boost::program_options;

namespace {
class interactive_card_reader {
    musicbox::controller *ctrl = nullptr;
    boost::asio::posix::stream_descriptor input;
    boost::asio::streambuf input_buffer;

   public:
    explicit interactive_card_reader(boost::asio::io_context &io) noexcept
        : input{io, ::dup(STDIN_FILENO)}, input_buffer{1024} {
    }

    void on_read(musicbox::controller *ctrl) { this->ctrl = ctrl; }

    void start() {
        boost::asio::async_read_until(input, input_buffer, '\n',
                                      boost::bind(&interactive_card_reader::handle_read_input, this,
                                                  boost::asio::placeholders::error,
                                                  boost::asio::placeholders::bytes_transferred));
    }

    void handle_read_input(const boost::system::error_code &error, std::size_t length) {
        if (!error) {
            musicbox::card_id_t id(length - 1);
            input_buffer.sgetn(reinterpret_cast<char *>(id.data()), length - 1); // NOLINT
            input_buffer.consume(1);  // Remove newline from input.

            if (ctrl != nullptr) {
                ctrl->card_approached(id);
            }

            start();
        } else if (error == boost::asio::error::not_found) {
            // Didn't get a newline, typically if the buffer is full, thus more than 1024 chars
        } else {
            //close();
        }
    }
};
}  // namespace

#include "MFRC522.h"

namespace {
class bcm_card_reader {
    musicbox::controller *ctrl = nullptr;
    MFRC522 mfrc;

   public:
    bcm_card_reader() {
        mfrc.PCD_Init();
    }

    void on_read(musicbox::controller *ctrl) {
        this->ctrl = ctrl;
    }

    void start() {
    }

};
}  // namespace

namespace {
void handle_signal(int signal) {
    if (signal != SIGINT && signal != SIGTERM) {
        // shouldn't be here ...
        std::terminate();
    }
    std::cout << "Terminating";
    ioc.stop();
}
}  // namespace

static interactive_card_reader reader{ioc};

int main(int argc, char *argv[]) {
    std::string bind_address{};
    unsigned short bind_port;
    musicbox::cli<musicbox::player::player::po, musicbox::storage::sqlite_po> cli{};

    po::options_description server_options("Configuration server options");
    server_options.add_options()("bind-address",
                                 po::value<std::string>(&bind_address)->default_value("0.0.0.0"),
                                 "network address to bind server to")(
        "bind-port", po::value<unsigned short>(&bind_port)->default_value(8008),
        "nework port to bind server to");

    cli.add(server_options);

    auto vm = cli(argc, argv);
    // bcm_card_reader reade2{};

    musicbox::player::player player{cli.get<0>()};
    musicbox::storage::sqlite_storage storage{cli.get<1>()};

    musicbox::my_logger logger;
    auto controller = musicbox::main_controller(player, storage, logger);
    reader.on_read(&controller);

    musicbox::http::configuration config{"www", &controller};
    serve(ioc, config, bind_address, 8008);
    reader.start();

    threads.reserve(thread_count - 1);
    for (auto i = thread_count - 1; i > 0; --i) {
        threads.emplace_back([] { ioc.run(); });
    }

    std::signal(SIGINT, handle_signal);
    std::signal(SIGTERM, handle_signal);

    ioc.run();
}

