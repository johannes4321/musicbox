#pragma once

#include <chrono>
#include <iosfwd>
#include <string>
#include <vector>

#include <absl/types/variant.h>

// TODO(johannes): what a hack, we should have the path in the include path
#include "../lib/json/include/nlohmann/json_fwd.hpp"


namespace musicbox {
using card_id_t = std::vector<uint8_t>;

struct empty_card {
    card_id_t card_id;
};

inline bool operator==(const empty_card& lhs, const empty_card& rhs) {
    return lhs.card_id == rhs.card_id;
}

struct song_card {
    int64_t id;
    card_id_t card_id;
    std::string card_name;
    std::string song_title;
    std::string song_ref;
    bool may_override;
};

inline bool operator==(const song_card& lhs, const song_card& rhs) {
    return lhs.id == rhs.id && lhs.card_id == rhs.card_id && lhs.card_name == rhs.card_name &&
           lhs.song_title == rhs.song_title && lhs.song_ref == rhs.song_ref &&
           lhs.may_override == rhs.may_override;
}

struct playlist_card {
    int64_t id;
    card_id_t card_id;
    std::string card_name;
    std::string list_title;
    std::string list_ref;
    bool may_override;
};

inline bool operator==(const playlist_card& lhs, const playlist_card& rhs) {
    return lhs.id == rhs.id && lhs.card_id == rhs.card_id && lhs.card_name == rhs.card_name &&
           lhs.list_title == rhs.list_title && lhs.list_ref == rhs.list_ref &&
           lhs.may_override == rhs.may_override;
}

struct silence_card {
    int64_t id;
    card_id_t card_id;
    std::string card_name;
    std::chrono::seconds timeout;
};

inline bool operator==(const silence_card& lhs, const silence_card& rhs) {
    return lhs.id == rhs.id && lhs.card_id == rhs.card_id && lhs.card_name == rhs.card_name &&
           lhs.timeout == rhs.timeout;
}

using card_variant = absl::variant<empty_card, song_card, playlist_card, silence_card>;

inline bool operator==(const card_variant& lhs, const card_variant& rhs) {
    if (lhs.index() != rhs.index()) {
        return false;
    }

    if (lhs.index() == 0) {
        return absl::get<0>(lhs) == absl::get<0>(rhs);
    } else if (lhs.index() == 1) {
        return absl::get<1>(lhs) == absl::get<1>(rhs);
    } else if (lhs.index() == 2) {
        return absl::get<2>(lhs) == absl::get<2>(rhs);
    } else if (lhs.index() == 3) {
        return absl::get<3>(lhs) == absl::get<3>(rhs);
    }
    static_assert(absl::variant_size<card_variant>() == 4);
    return false;
}

inline bool operator!=(const card_variant& lhs, const card_variant& rhs) {
    return !(lhs == rhs);
}

card_id_t card_id(const card_variant& card);

void to_json(nlohmann::json& j, const card_variant& card);
void to_json(nlohmann::json& j, const empty_card& card);
void to_json(nlohmann::json& j, const song_card& card);
void to_json(nlohmann::json& j, const playlist_card& card);
void to_json(nlohmann::json& j, const silence_card& card);

void from_json(const nlohmann::json& j, card_variant& card);
void from_json(const nlohmann::json& j, empty_card& card);
void from_json(const nlohmann::json& j, song_card& card);
void from_json(const nlohmann::json& j, playlist_card& card);
void from_json(const nlohmann::json& j, silence_card& card);

std::ostream& card_id_to_stream(std::ostream& s, const card_id_t& id);
std::ostream& operator<<(std::ostream& s, const musicbox::empty_card& card);
std::ostream& operator<<(std::ostream& s, const musicbox::song_card& card);
std::ostream& operator<<(std::ostream& s, const musicbox::playlist_card& card);
std::ostream& operator<<(std::ostream& s, const musicbox::silence_card& card);
std::ostream& operator<<(std::ostream& s, const musicbox::card_variant& card);
}  // namespace musicbox

