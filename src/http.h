#pragma once

#include "controller.h"

#include <string>

namespace boost {
namespace asio {
class io_context;
}
}

namespace musicbox {
namespace http {
struct configuration {
    // TODO(johannes): Follow Google's advice?
    const std::string& doc_root;  // NOLINT(google-runtime-member-string-references)
    controller* controller;
};

class websocket_session {
   public:
    virtual void send(const std::string& data) = 0;
    virtual ~websocket_session() = default;
};

void serve(boost::asio::io_context& ioc, configuration& config, const std::string& bind_address,
           unsigned short port);

}  // namespace http
}  // namespace musicbox
