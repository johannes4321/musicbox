#pragma once

#include "cards.h"

#include <absl/strings/string_view.h>
#include <memory>
#include <vector>

namespace musicbox {

namespace http {
class websocket_session;
}

class controller {
    virtual void end_config() = 0;

   public:
    class configuration {
        controller *ctrl;
        std::weak_ptr<http::websocket_session> websocket;

       public:
        using ptr = std::shared_ptr<configuration>;

        explicit configuration(controller *ctrl) : ctrl(ctrl) {
        }

        configuration(const configuration &) = delete;
        configuration(configuration &&) = delete;
        void operator=(const configuration &) = delete;
        void operator=(configuration &&) = delete;

        ~configuration() {
            ctrl->end_config();
        }

        void connect(std::shared_ptr<http::websocket_session> websocket);

        void receive_command(absl::string_view data);

        void send_card(const card_variant &card);
    };
    virtual configuration::ptr start_config() = 0;

    virtual std::vector<card_variant> get_all_cards() = 0;

    virtual bool store(const card_variant& card) = 0;

    virtual void card_approached(const card_id_t &id) = 0;

    friend configuration;
};
}  // namespace musicbox
