#include "../player/player.h"

#include "../cli.h"

namespace po = boost::program_options;

int main(int argc, char *argv[]) {
    po::options_description optplayer("Player options");
    optplayer.add_options()
      ("song,s", po::value<std::string>(), "play a song")
      ("list,l", po::value<std::string>(), "play a playlist");

    musicbox::cli<musicbox::player::player::po> cli{};
    cli.add(optplayer);
    auto vm = cli(argc, argv);

    musicbox::player::player player{cli.get<0>()};

    auto id_title = player.get_current_id_and_title();
    std::cout << "Playing: " << id_title.second << " (" << id_title.first << ")\n";

    std::cout << "Playlists:\n";
    auto pls = player.get_playlists();
    for (auto &&p : pls) {
        std::cout << "  " << p.first << "\n";
    }
    std::cout << "\n";

    if (vm.count("song")) {
        std::string song{vm["song"].as<std::string>()};
        std::cout << "Starting: " << song << " " << (player.play(song) ? "SUCCESS" : "FAILURE")
                  << "\n";
    }
    if (vm.count("list")) {
        std::string list{vm["list"].as<std::string>()};
        std::cout << "Starting: " << list << " " << (player.play_list(list) ? "SUCCESS" : "FAILURE")
                  << "\n";
    }
}

