#include <iostream>

#include "../storage/sqlite_storage.h"
#include "../cli.h"
#include "nlohmann/json.hpp"

namespace po = boost::program_options;

std::ostream& operator<<(std::ostream& s, const musicbox::card_id_t& id) {
    return musicbox::card_id_to_stream(s, id);
}

int main(int argc, char* argv[]) {
    musicbox::cli<musicbox::storage::sqlite_po> cli{};

    bool json = false;
    bool all = true;  // false;
    bool add_song = false;
    bool add_list = false;
    bool add_silence = false;

    po::options_description query_options("Query options");
    query_options.add_options()("json,j", po::value<bool>(&json)->implicit_value(true),
                                "Output in JSON format")(
        "all,a", po::value<bool>(&all)->implicit_value(true), "show all stored tokens")(
        "add-song,s", po::value<bool>(&add_song)->implicit_value(true), "Add a Song Card")(
        "add-list,l", po::value<bool>(&add_list)->implicit_value(true), "Add a List Card")(
        "add-silence,i", po::value<bool>(&add_silence)->implicit_value(true), "Add a Silence Card");

    cli.add(query_options);

    cli(argc, argv);

    musicbox::storage::sqlite_storage storage{cli.get<0>()};

    if (add_song) {
        musicbox::song_card card;
        std::string id;
        std::cout << "New card id: ";
        std::getline(std::cin, id);
        card.card_id = {id.begin(), id.end()};
        std::cout << "Card name: ";
        std::getline(std::cin, card.card_name);
        std::cout << "Song title: ";
        std::getline(std::cin, card.song_title);
        std::cout << "Song reference: ";
        std::getline(std::cin, card.song_ref);
        std::cout << "May override? ";
        std::cin >> card.may_override;

        auto result = storage.store(card);
        std::cout << "Result: " << (result ? "success" : "failure") << "\n\n";
    }
    if (add_list) {
        musicbox::playlist_card card;
        std::string id;
        std::cout << "New card id: ";
        std::getline(std::cin, id);
        card.card_id = {id.begin(), id.end()};
        std::cout << "Card name: ";
        std::getline(std::cin, card.card_name);
        std::cout << "List title: ";
        std::getline(std::cin, card.list_title);
        std::cout << "List reference: ";
        std::getline(std::cin, card.list_ref);
        std::cout << "May override? ";
        std::cin >> card.may_override;

        auto result = storage.store(card);
        std::cout << "Result: " << (result ? "success" : "failure") << "\n\n";
    }

    if (add_silence) {
        musicbox::silence_card card;
        int timeout;
        std::string id;
        std::cout << "New card id: ";
        std::getline(std::cin, id);
        card.card_id = {id.begin(), id.end()};
        std::cout << "Card name: ";
        std::getline(std::cin, card.card_name);
        std::cout << "Timeout (s): ";
        std::cin >> timeout;
        card.timeout = std::chrono::seconds{timeout};

        auto result = storage.store(card);
        std::cout << "Result: " << (result ? "success" : "failure") << "\n\n";
    }

    if (all) {
        auto cards = storage.get_all_cards();
        if (json) {
            nlohmann::json j = cards;
            std::cout << j.dump(2);
        } else {
            for (auto& card : cards) {
                std::cout << card;
            }
        }
        return 0;
    }

    do {
        std::string id;
        std::cout << "Query ID: ";
        std::getline(std::cin, id);
        std::vector<uint8_t> card_id{id.begin(), id.end()};
        std::cout << "Searching " << card_id << "\n";
        auto card = storage.get_card(card_id);
        if (json) {
            nlohmann::json j = card;
            std::cout << j.dump(2);
        } else {
            std::cout << card;
        }
        std::cout << '\n';
  } while (!std::cin.eof());
}
