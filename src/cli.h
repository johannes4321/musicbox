#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <tuple>
#include <vector>

#include "../musicbox_config.h"

namespace musicbox {
namespace {
inline void version_info() noexcept {
    std::cout << "musicbox " << __DATE__ << " " << __TIME__ << " " << musicbox::GIT_BRANCH << "/"
              << musicbox::GIT_COMMIT_HASH << "\n";
}
}  // namespace

template <typename... T>
class cli {
    boost::program_options::options_description desc;
    boost::program_options::variables_map vm;
    std::tuple<T...> modules;

    void parse_configs() {
        namespace po = boost::program_options;
        std::vector<std::string> paths{"/etc/musicbox.ini"};
        auto home = getenv("HOME");
        if (home) {
	    auto user = std::string{} + home + "/.config/musicbox.ini";
            paths.push_back(user);
        }
        paths.push_back("musicbox.ini");

        for (auto &&path : paths) {
            std::ifstream cfg_file{path};
            if (cfg_file) {
                po::store(po::parse_config_file<char>(cfg_file, desc, true), vm);
            }
        }
    }

   public:
    explicit cli() : desc{}, vm{}, modules{} {
        namespace po = boost::program_options;
        po::options_description generic("Generic options");
        generic.add_options()("help,h", "show help");
	generic.add_options()("version,v", "show version info");
        desc.add(generic);

	(desc.add(std::get<T>(modules).commandline_options()), ...);
    }

    void add(const boost::program_options::options_description &d) {
        desc.add(d);
    }

    boost::program_options::variables_map operator()(int argc, char *argv[]) {
        namespace po = boost::program_options;
	parse_configs();
        po::store(po::parse_command_line(argc, argv, desc), vm);

        po::notify(vm);

        if (vm.count("version")) {
            version_info();
            std::exit(0);
        }

        if (vm.count("help")) {
            std::cout << desc << "\n";
            std::exit(0);
        }
        return vm;
    }

    template <typename M>
    auto get() const try {
        return std::get<M>(modules).create_instance();
    } catch (const std::exception &e) {
        std::cerr << "Error while creating " << M::name << ": " << e.what() << "\n";
        std::exit(1);
    }

    template <size_t i>
    auto get() const try {
        return std::get<i>(modules).create_instance();
    } catch (const std::exception &e) {
        using M = typename std::tuple_element<i, decltype(modules)>::type;
        std::cerr << "Error while creating " << M::name << ": " << e.what() << "\n";
        std::exit(1);
    }
};
}  // namespace musicbox
