#include "cards.h"

#include "nlohmann/json.hpp"

#include <iostream>

namespace musicbox {
using nlohmann::json;

namespace {
struct card_id_visitor {
    template <typename card_t>
    card_id_t operator()(const card_t& card) const {
        return card.card_id;
    }
};
}  // namespace

card_id_t card_id(const card_variant& card) {
    return absl::visit(card_id_visitor{}, card);
}

namespace {
struct card_json_visitor {
    nlohmann::json& j;

    template <typename card_t>
    void operator()(const card_t& card) const {
        to_json(j, card);
    }
};
}  // namespace

void to_json(nlohmann::json& j, const card_variant& card) {
    absl::visit(card_json_visitor{j}, card);
}

void to_json(nlohmann::json& j, const empty_card& card) {
    j = json{{"type", "empty"}, {"card_id", card.card_id}};
}

void to_json(nlohmann::json& j, const song_card& card) {
    j = json{{"type", "song"},
             {"card_id", card.card_id},
             {"id", card.id},
             {"card_name", card.card_name},
             {"song_title", card.song_title},
             {"song_ref", card.song_ref},
             {"may_override", card.may_override}};
}

void to_json(nlohmann::json& j, const playlist_card& card) {
    j = json{{"type", "playlist"},
             {"card_id", card.card_id},
             {"id", card.id},
             {"card_name", card.card_name},
             {"list_title", card.list_title},
             {"list_ref", card.list_ref},
             {"may_override", card.may_override}};
}
void to_json(nlohmann::json& j, const silence_card& card) {
    j = json{{"type", "silence"},
             {"card_id", card.card_id},
             {"id", card.id},
             {"card_name", card.card_name},
             {"timeout", card.timeout.count()}};
}

void from_json(const nlohmann::json& j, card_variant& card) {
    auto card_type = j["type"].get<std::string>();

    if (card_type == "song") {
        card = j.get<song_card>();
    } else if (card_type == "playlist") {
        card = j.get<playlist_card>();
    } else if (card_type == "silence") {
        card = j.get<silence_card>();
    } else if (card_type == "empty") {
        card = j.get<empty_card>();
    } else {
        throw std::runtime_error{"invalid card type"};
    }
}

void from_json(const nlohmann::json& j, empty_card& card) {
    j.at("card_id").get_to<card_id_t>(card.card_id);
}

void from_json(const nlohmann::json& j, song_card& card) {
    j.at("card_id").get_to(card.card_id);
    j.at("id").get_to(card.id);
    j.at("card_name").get_to(card.card_name);
    j.at("song_title").get_to(card.song_title);
    j.at("song_ref").get_to(card.song_ref);
    j.at("may_override").get_to(card.may_override);
}

void from_json(const nlohmann::json& j, playlist_card& card) {
    j.at("card_id").get_to(card.card_id);
    j.at("id").get_to(card.id);
    j.at("card_name").get_to(card.card_name);
    j.at("list_title").get_to(card.list_title);
    j.at("list_ref").get_to(card.list_ref);
    j.at("may_override").get_to(card.may_override);
}

void from_json(const nlohmann::json& j, silence_card& card) {
    j.at("card_id").get_to(card.card_id);
    j.at("id").get_to(card.id);
    j.at("card_name").get_to(card.card_name);
    std::chrono::seconds timeout{j.at("timeout").get<int>()};
    card.timeout = timeout;
}

std::ostream& card_id_to_stream(std::ostream& s, const card_id_t& id) {
    bool first = true;
    for (auto value : id) {
        if (first) {
            first = false;
        } else {
            s << ":";
        }
        s << std::hex << (int)value << std::dec;
    }

    return s;
}

std::ostream& operator<<(std::ostream& s, const musicbox::empty_card& card) {
    return card_id_to_stream(s, card.card_id) << " is empty\n";
}

std::ostream& operator<<(std::ostream& s, const musicbox::silence_card& card) {
    s << "Silence Card #" << card.id << "\n"
      << "\t";
    card_id_to_stream(s, card.card_id);
    s << "\n"
      << "\t" << card.card_name << "\n"
      << "\t" << card.timeout.count() << "s\n";
    return s;
}

std::ostream& operator<<(std::ostream& s, const musicbox::song_card& card) {
    s << "Song Card #" << card.id << "\n"
      << "\t";
    card_id_to_stream(s, card.card_id);
    s << "\n"
      << "\t" << card.card_name << "\n"
      << "\t" << card.song_title << "\n"
      << "\t" << card.song_ref << "\n";
    return s;
}

std::ostream& operator<<(std::ostream& s, const musicbox::playlist_card& card) {
    s << "Playlist Card #" << card.id << "\n"
      << "\t";
    card_id_to_stream(s, card.card_id);
    s << "\n"
      << "\t" << card.card_name << "\n"
      << "\t" << card.list_title << "\n"
      << "\t" << card.list_ref << "\n";
    return s;
}

class card_print_visitor {
    std::ostream& s;

   public:
    card_print_visitor(std::ostream& s) : s{s} {
    }

    template <typename card_t>
    void operator()(const card_t& card) const {
        s << card;
    }
};

std::ostream& operator<<(std::ostream& s, const musicbox::card_variant& card) {
    absl::visit(card_print_visitor{s}, card);
    return s;
}
}  // namespace musicbox
