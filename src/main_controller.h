#pragma once

#include "cards.h"
#include "controller.h"

#include <boost/sml.hpp>

#include <iostream>
#include <mutex>

namespace musicbox {
namespace {
namespace sml = boost::sml;

struct my_logger {
    template <class SM, class TEvent>
    void log_process_event([[maybe_unused]] const TEvent &e) {
        std::cout << '[' /*<< sml::aux::get_type_name<SM>()*/ << "process_event] "
                  << sml::aux::get_type_name<TEvent>() << '\n';
    }

    template <class SM, class TGuard, class TEvent>
    void log_guard([[maybe_unused]] const TGuard &guard, [[maybe_unused]] const TEvent &e,
                   bool result) {
        std::cout << '[' /*<< sml::aux::get_type_name<SM>()*/ << "][guard]"
                  << sml::aux::get_type_name<TGuard>() << ", " << sml::aux::get_type_name<TEvent>()
                  << ", " << (result ? "[OK]" : "[Reject]") << '\n';
    }

    template <class SM, class TAction, class TEvent>
    void log_action([[maybe_unused]] const TAction &a, [[maybe_unused]] const TEvent &e) {
        std::cout << '[' /*<< sml::aux::get_type_name<SM>()*/ << "][action]"
                  << sml::aux::get_type_name<TAction>() << ' ' << sml::aux::get_type_name<TEvent>()
                  << '\n';
    }

    template <class SM, class TSrcState, class TDstState>
    void log_state_change(const TSrcState &src, const TDstState &dst) {
        std::cout << '[' /*<< sml::aux::get_type_name<SM>()*/ << "][transition]" << src.c_str()
                  << " -> " << dst.c_str() << '\n';
    }
};

struct null_logger {
    template <class SM, class TEvent>
    void log_process_event([[maybe_unused]] const TEvent &) {
    }

    template <class SM, class TGuard, class TEvent>
    void log_guard([[maybe_unused]] const TGuard &, [[maybe_unused]] const TEvent &, bool) {
    }

    template <class SM, class TAction, class TEvent>
    void log_action([[maybe_unused]] const TAction &, [[maybe_unused]] const TEvent &) {
    }

    template <class SM, class TSrcState, class TDstState>
    void log_state_change(const TSrcState &, const TDstState &) {
    }
};
}  // namespace

template <typename Player, typename Storage, typename Logger>
class main_controller : public musicbox::controller {
    Player &player;
    Storage &storage;
    std::weak_ptr<controller::configuration> config;

    class music_state {
        main_controller *ctrl;

       public:
        music_state(main_controller *ctrl) : ctrl{ctrl} {
        }

        void play_song_action(const song_card &card) {
            ctrl->player.play(card.song_ref);
        }

        void play_list_action(const playlist_card &card) {
            ctrl->player.play_list(card.list_ref);
        }

        void configure_card_action(const card_variant &card) {
            auto config = ctrl->config.lock();
            if (config) {
                config->send_card(card);
            }
        };

        auto operator()() {
            using namespace boost::sml;  // NOLINT

	    /*
            auto configure_card_action = [](const card_touched &card) {
                auto id = card_id(card.card);
		//ctrl->is_idle();
                std::cout
                    << card.card;  // "action1, length of card id: " << id.size() << std::endl;
            };
	    */

            // clang-format off
            return make_transition_table(
                *"idle"_s         + event<empty_card>                         = "idle"_s,
                *"idle"_s         + event<song_card>
                         / [this](const song_card &c) { play_song_action(c); }     = "idle"_s, //"playing"_s,
                *"idle"_s         + event<playlist_card>
                         / [this](const playlist_card &c) { play_list_action(c); } = "idle"_s, //"playing"_s,
                *"idle"_s         + event<silence_card>                       = "silence"_s, 
                "playing"_s       + "timer"_e                                 = "idle"_s,
                "silence"_s       + "timer"_e                                 = "idle"_s, 
                "idle"_s          + "start_config"_e                          = "configuration"_s,
                "configuration"_s + event<card_variant>
                         / [this](const card_variant &c) { configure_card_action(c); } = "configuration"_s,
                "configuration"_s + "end_config"_e                            = "idle"_s
            );
	    // clang-format on
        }
    };

    boost::sml::sm<music_state, boost::sml::logger<Logger>, sml::thread_safe<std::recursive_mutex>> sm;

   public:
    explicit main_controller(Player &player, Storage &storage, Logger &logger)
        : player{player}, storage{storage}, config{}, sm{music_state{this}, logger} {
    }

    configuration::ptr start_config() override {
        using namespace boost::sml;  // NOLINT
        sm.process_event("start_config"_e());
        auto config = std::make_shared<configuration>(this);
	this->config = config;
	return config;
    }

    void end_config() override {
        using namespace boost::sml;  // NOLINT
        sm.process_event("end_config"_e());
	config.reset();
    }

    std::vector<card_variant> get_all_cards() override {
        return storage.get_all_cards();
    }

    bool store(const card_variant &card) override {
        return storage.store(card);
    }

    class invoke_sm_event {
        decltype(sm) &smr;

       public:
        invoke_sm_event(decltype(sm) &smr) : smr{smr} {
        }

        template <typename card_t>
        void operator()(const card_t &card) {
            smr.process_event(card);
        }
    };

    void card_approached(const card_id_t &id) override {
        auto card = storage.get_card(id);
	absl::visit(invoke_sm_event{sm}, card);
	sm.process_event(card);
    }

    bool is_idle() const noexcept {
        using namespace boost::sml;
        return sm.is("idle"_s);
    }
    bool is_in_config() const noexcept {
        using namespace boost::sml;
        return sm.is("configuration"_s);
    }
};
}
