
#define BOOST_ERROR_CODE_HEADER_ONLY
#include "http.h"

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include <boost/asio/bind_executor.hpp>
#include <boost/asio/buffer.hpp>
#include <boost/asio/buffers_iterator.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/asio/strand.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/config.hpp>

#include "config.h"

namespace musicbox {
namespace http {

using tcp = boost::asio::ip::tcp;
namespace asio = boost::asio;
namespace beast = boost::beast;
namespace http = boost::beast::http;
namespace websocket = boost::beast::websocket;

namespace {
const char* SERVER_NAME = MUSICBOX_NAME "/" MUSICBOX_VERSION;

// Return a reasonable mime type based on the extension of a file.
beast::string_view mime_type(beast::string_view path) {
    using beast::iequals;
    auto const ext = [&path] {
        auto const pos = path.rfind(".");
        if (pos == beast::string_view::npos) {
            return beast::string_view{};
        }
        return path.substr(pos);
    }();
    // clang-format off
    if (iequals(ext, ".htm"))  { return "text/html"; }
    if (iequals(ext, ".html")) { return "text/html"; }
    if (iequals(ext, ".css"))  { return "text/css"; }
    if (iequals(ext, ".txt"))  { return "text/plain"; }
    if (iequals(ext, ".js"))   { return "application/javascript"; }
    if (iequals(ext, ".json")) { return "application/json"; }
    if (iequals(ext, ".xml"))  { return "application/xml"; }
    if (iequals(ext, ".png"))  { return "image/png"; }
    if (iequals(ext, ".jpe"))  { return "image/jpeg"; }
    if (iequals(ext, ".jpeg")) { return "image/jpeg"; }
    if (iequals(ext, ".jpg"))  { return "image/jpeg"; }
    if (iequals(ext, ".gif"))  { return "image/gif"; }
    if (iequals(ext, ".bmp"))  { return "image/bmp"; }
    if (iequals(ext, ".ico"))  { return "image/vnd.microsoft.icon"; }
    if (iequals(ext, ".svg"))  { return "image/svg+xml"; }
    if (iequals(ext, ".svgz")) { return "image/svg+xml"; }
    return "application/text";
    // clang-format on
}

// Append an HTTP rel-path to a local filesystem path.
// The returned path is normalized for the platform.
std::string path_cat(beast::string_view base, beast::string_view path) {
    if (base.empty()) {
        return path.to_string();
    }
    std::string result = base.to_string();
    if (result.back() == '/') {
        result.resize(result.size() - 1);
    }
    result.append(path.data(), path.size());
    return result;
}

// This function produces an HTTP response for the given
// request. The type of the response object depends on the
// contents of the request, so the interface requires the
// caller to pass a generic lambda for receiving the response.
template <class Body, class Allocator, class Send>
void handle_request(beast::string_view doc_root,
                    http::request<Body, http::basic_fields<Allocator>>&& req, Send&& send) {
    // Returns a bad request response
    auto const bad_request = [&req](beast::string_view why) {
        http::response<http::string_body> res{http::status::bad_request, req.version()};
        res.set(http::field::server, SERVER_NAME);
        res.set(http::field::content_type, "text/html");
        res.keep_alive(req.keep_alive());
        res.body() = why.to_string();
        res.prepare_payload();
        return res;
    };

    // Returns a not found response
    auto const not_found = [&req](beast::string_view target) {
        http::response<http::string_body> res{http::status::not_found, req.version()};
        res.set(http::field::server, SERVER_NAME);
        res.set(http::field::content_type, "text/html");
        res.keep_alive(req.keep_alive());
        res.body() = "The resource '" + target.to_string() + "' was not found.";
        res.prepare_payload();
        return res;
    };

    // Returns a server error response
    auto const server_error = [&req](beast::string_view what) {
        http::response<http::string_body> res{http::status::internal_server_error, req.version()};
        res.set(http::field::server, SERVER_NAME);
        res.set(http::field::content_type, "text/html");
        res.keep_alive(req.keep_alive());
        res.body() = "An error occurred: '" + what.to_string() + "'";
        res.prepare_payload();
        return res;
    };

    if (req.method() != http::verb::get && req.method() != http::verb::head) {
        return send(bad_request("Unknown HTTP-method"));
    }

    // Request path must be absolute and not contain "..".
    if (req.target().empty() || req.target()[0] != '/' ||
        req.target().find("..") != beast::string_view::npos) {
        return send(bad_request("Illegal request-target"));
    }

    // Build the path to the requested file
    std::string path = path_cat(doc_root, req.target());
    if (req.target().back() == '/') {
        path.append("index.html");
    }

    // Attempt to open the file
    beast::error_code ec;
    http::file_body::value_type body;
    body.open(path.c_str(), beast::file_mode::scan, ec);

    // Handle the case where the file doesn't exist
    if (ec == boost::system::errc::no_such_file_or_directory) {
        return send(not_found(req.target()));
    }

    // Handle an unknown error
    if (ec) {
        return send(server_error(ec.message()));
    }

    if (req.method() == http::verb::head) {
        http::response<http::empty_body> res{http::status::ok, req.version()};
        res.set(http::field::server, SERVER_NAME);
        res.set(http::field::content_type, mime_type(path));
        res.content_length(body.size());
        res.keep_alive(req.keep_alive());
        return send(std::move(res));
    }

    // Respond to GET request
    auto size = body.size();
    http::response<http::file_body> res{std::piecewise_construct, std::make_tuple(std::move(body)),
                                        std::make_tuple(http::status::ok, req.version())};
    res.set(http::field::server, SERVER_NAME);
    res.set(http::field::content_type, mime_type(path));
    res.content_length(size);
    res.keep_alive(req.keep_alive());
    return send(std::move(res));
}

//------------------------------------------------------------------------------

// Report a failure
void fail(boost::system::error_code ec, char const* what) {
    std::cerr << what << ": " << ec.message() << "\n";
}

class websocket_session : public std::enable_shared_from_this<websocket_session>,
	public ::musicbox::http::websocket_session {
    websocket::stream<tcp::socket> ws;
    asio::strand<asio::io_context::executor_type> strand;
    beast::flat_buffer buffer;
    controller::configuration::ptr ctrl_config;

   public:
    // Take ownership of the socket
    explicit websocket_session(tcp::socket socket, const configuration& config)
        : ws(std::move(socket)),
          strand(ws.get_executor()),
          ctrl_config{config.controller->start_config()} {
        assert(ctrl_config); // NOLINT
    }

    // Start the asynchronous operation
    template <class Body, class Allocator>
    void run(http::request<Body, http::basic_fields<Allocator>> req) {
        // Accept the websocket handshake
        ws.async_accept(
            req, asio::bind_executor(strand, std::bind(&websocket_session::on_accept,
                                                       shared_from_this(), std::placeholders::_1)));
	ctrl_config->connect(shared_from_this());
    }
    
    void send(const std::string& data) override {
        asio::const_buffer write_buffer = asio::buffer(data);
        ws.text(ws.got_text());
	ws.write(write_buffer);
    }

    void on_accept(boost::system::error_code ec) {
        // Happens when the timer closes the socket
        if (ec == asio::error::operation_aborted) {
            return;
        }

        if (ec) {
            return fail(ec, "accept");
        }

        // Read a message
        do_read();
    }

    void do_read() {
        // Read a message into our buffer
        ws.async_read(buffer, asio::bind_executor(
                                  strand, std::bind(&websocket_session::on_read, shared_from_this(),
                                                    std::placeholders::_1, std::placeholders::_2)));
    }

    void on_read(boost::system::error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);

        // Happens when the timer closes the socket
        if (ec == asio::error::operation_aborted) {
            return;
        }

        // This indicates that the websocket_session was closed
        if (ec == websocket::error::closed) {
            return;
        }

        if (ec) {
            fail(ec, "read");
        }

        if (ctrl_config) {
            ctrl_config->receive_command(
                {reinterpret_cast<const char*>(buffer.data().data()), buffer.size()});
        }

        buffer.consume(buffer.size());
	do_read();
    }

    void on_write(boost::system::error_code ec, std::size_t bytes_transferred) {
        boost::ignore_unused(bytes_transferred);

        // Happens when the timer closes the socket
        if (ec == asio::error::operation_aborted) {
            return;
        }

        if (ec) {
            return fail(ec, "write");
        }

        // Clear the buffer
        buffer.consume(buffer.size());

        // Do another read
        do_read();
    }
};

// Handles an HTTP server connection
class http_session : public std::enable_shared_from_this<http_session> {
    // This queue is used for HTTP pipelining.
    class queue {
        enum {
            // Maximum number of responses we will queue
            limit = 8
        };

        // The type-erased, saved work item
        struct work {  // NOLINT
            virtual ~work() = default;
            virtual void operator()() = 0;
        };
        http_session& self;
        std::vector<std::unique_ptr<work>> items;

       public:
        explicit queue(http_session& self)  // NOLINT(google-runtime-references)
            : self(self) {
            static_assert(limit > 0, "queue limit must be positive");
            items.reserve(limit);
        }

        bool is_full() const { return items.size() >= limit; }

        // Called when a message finishes sending
        // Returns `true` if the caller should initiate a read
        bool on_write() {
            BOOST_ASSERT(!items.empty());  // NOLINT
            auto const was_full = is_full();
            items.erase(items.begin());
            if (!items.empty()) {
                (*items.front())();
            }
            return was_full;
        }

        // Called by the HTTP handler to send a response.
        template <bool isRequest, class Body, class Fields>
        void operator()(http::message<isRequest, Body, Fields>&& msg) {
            // This holds a work item
            struct work_impl : work {
                http_session& self;
                http::message<isRequest, Body, Fields> msg;

                work_impl(http_session& self,  // NOLINT(google-runtime-references)
                          http::message<isRequest, Body, Fields>&& msg)
                    : self(self), msg(std::move(msg)) {}

                void operator()() override {
                    http::async_write(
                        self.socket, msg,
                        asio::bind_executor(
                            self.strand, std::bind(&http_session::on_write, self.shared_from_this(),
                                                   std::placeholders::_1, msg.need_eof())));
                }
            };

            // Allocate and store the work
            items.emplace_back(new work_impl(self, std::move(msg)));  // NOLINT

            // If there was no previous work, start this one
            if (items.size() == 1) {
                (*items.front())();
            }
        }
    };

    tcp::socket socket;
    asio::strand<asio::io_context::executor_type> strand;
    asio::steady_timer timer;
    beast::flat_buffer buffer;
    const configuration& config;
    http::request<http::string_body> req;
    queue queue;

   public:
    // Take ownership of the socket
    explicit http_session(tcp::socket socket, const configuration& config)
        : socket(std::move(socket)),
          strand(socket.get_executor()),
          timer(socket.get_executor().context(), (std::chrono::steady_clock::time_point::max)()),
          config(config),
          queue(*this) {}

    // Start the asynchronous operation
    void run() {
        // Run the timer. The timer is operated
        // continuously, this simplifies the code.
        on_timer({});

        do_read();
    }

    void do_read() {
        timer.expires_after(std::chrono::seconds(15));

        // Read a request
        http::async_read(
            socket, buffer, req,
            asio::bind_executor(strand, std::bind(&http_session::on_read, shared_from_this(),
                                                         std::placeholders::_1)));
    }

    // Called when the timer expires.
    void on_timer(boost::system::error_code ec) {
        if (ec && ec != asio::error::operation_aborted) {
            return fail(ec, "timer");
        }

        // Verify that the timer really expired since the deadline may have moved.
        if (timer.expiry() <= std::chrono::steady_clock::now()) {
            // Closing the socket cancels all outstanding operations. They
            // will complete with asio::error::operation_aborted
            socket.shutdown(tcp::socket::shutdown_both, ec);
            socket.close(ec);
            return;
        }

        // Wait on the timer
        timer.async_wait(asio::bind_executor(
            strand, std::bind(&http_session::on_timer, shared_from_this(), std::placeholders::_1)));
    }

    void on_read(boost::system::error_code ec) {
        // Happens when the timer closes the socket
        if (ec == asio::error::operation_aborted) {
            return;
        }

        // This means they closed the connection
        if (ec == http::error::end_of_stream) {
            return do_close();
        }

        if (ec) {
            return fail(ec, "read");
        }

        // See if it is a WebSocket Upgrade
        if (websocket::is_upgrade(req)) {
            // Create a WebSocket websocket_session by transferring the socket
            std::make_shared<websocket_session>(std::move(socket), config)->run(std::move(req));
            return;
        }

        // Send the response
        handle_request(config.doc_root, std::move(req), queue);

        // If we aren't at the queue limit, try to pipeline another request
        if (!queue.is_full()) {
            do_read();
        }
    }

    void on_write(boost::system::error_code ec, bool close) {
        // Happens when the timer closes the socket
        if (ec == asio::error::operation_aborted) {
            return;
        }

        if (ec) {
            return fail(ec, "write");
        }

        if (close) {
            // This means we should close the connection, usually because
            // the response indicated the "Connection: close" semantic.
            return do_close();
        }

        // Inform the queue that a write completed
        if (queue.on_write()) {
            // Read another request
            do_read();
        }
    }

    void do_close() {
        // Send a TCP shutdown
        boost::system::error_code ec;
        socket.shutdown(tcp::socket::shutdown_send, ec);

        // At this point the connection is closed gracefully
    }
};

//------------------------------------------------------------------------------

// Accepts incoming connections and launches the sessions
class listener : public std::enable_shared_from_this<listener> {
    tcp::acceptor acceptor;
    tcp::socket socket;
    const configuration& config;

   public:
    listener(asio::io_context& ioc, const tcp::endpoint& endpoint,
             const configuration& config)  // NOLINT
        : acceptor(ioc), socket(ioc), config(config) {
        boost::system::error_code ec;

        // Open the acceptor
        acceptor.open(endpoint.protocol(), ec);
        if (ec) {
            fail(ec, "open");
            return;
        }

        // Bind to the server address
        acceptor.bind(endpoint, ec);
        if (ec) {
            fail(ec, "bind");
            return;
        }

        // Start listening for connections
        acceptor.listen(asio::socket_base::max_listen_connections, ec);
        if (ec) {
            fail(ec, "listen");
            return;
        }
    }

    // Start accepting incoming connections
    void run() {
        if (!acceptor.is_open()) {
            return;
        }
        do_accept();
    }

    void do_accept() {
        acceptor.async_accept(
            socket, std::bind(&listener::on_accept, shared_from_this(), std::placeholders::_1));
    }

    void on_accept(boost::system::error_code ec) {
        if (ec) {
            fail(ec, "accept");
        } else {
            // Create the http_session and run it
            std::make_shared<http_session>(std::move(socket), config)->run();
        }

        // Accept another connection
        do_accept();
    }
};
}  // namespace

void serve(asio::io_context& ioc, configuration& config, const std::string& bind_address,
           unsigned short port) {
    auto const address = asio::ip::make_address(bind_address);

    // Create and launch a listening port
    std::make_shared<listener>(ioc, tcp::endpoint{address, port}, config)->run();

    std::vector<std::thread> v;
}
}  // namespace http
}  // namespace musicbox
