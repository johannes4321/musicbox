#pragma once

#include "../cards.h"

#include <boost/program_options.hpp>

struct sqlite3;
struct sqlite3_stmt;

namespace musicbox::storage {
class sqlite_storage;

class sqlite_po {
    std::string sqlite_file;

   public:
    constexpr static char name[] = "SQLite Storage";
    boost::program_options::options_description commandline_options();
    sqlite_storage create_instance() const;
};

class sqlite_storage {
   public:
    using stmt_ptr = std::unique_ptr<sqlite3_stmt, int (*)(sqlite3_stmt *)>;

    struct card_stmts {
        mutable stmt_ptr select_all;
        mutable stmt_ptr select_one;
        mutable stmt_ptr delete_one;
        mutable stmt_ptr insert;

	explicit card_stmts();
    };
    using card_stmts_t = std::array<card_stmts, 3>;
   private:
    std::unique_ptr<sqlite3, int (*)(sqlite3 *)> db;

    enum card_types { card_type_song = 0, card_type_playlist, card_type_silence, card_type_end };
    const card_stmts_t stmts;
    static_assert(std::tuple_size<card_stmts_t>::value == card_type_end);

   public:
    explicit sqlite_storage(const char *f);

    sqlite_storage(const sqlite_storage &) = delete;
    sqlite_storage(sqlite_storage &&) = delete;
    bool operator=(const sqlite_storage &) = delete;
    bool operator=(sqlite_storage &&) = delete;
    ~sqlite_storage() = default;

    std::vector<card_variant> get_all_cards() noexcept;
    card_variant get_card(const std::vector<uint8_t> &id) noexcept;

    bool store(const song_card &card) noexcept;
    bool store(const playlist_card &card) noexcept;
    bool store(const silence_card &card) noexcept;
    bool store(const card_variant &card) noexcept;
};
}  // namespace muscbox::storage
