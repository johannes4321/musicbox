#include "sqlite_storage.h"

#include <stdexcept>
#include <absl/types/optional.h>

#include "sqlite3.h"

namespace musicbox::storage {
namespace {
class sqlite_transaction_guard {
    sqlite3 *db;
    bool commited;

   public:
    sqlite_transaction_guard(sqlite3 *db) : db{db}, commited{false} {
        auto result = sqlite3_exec(db, "BEGIN", nullptr, nullptr, nullptr);
        if (result != SQLITE_OK) {
            throw std::runtime_error{sqlite3_errstr(result)};
        }
    }

    sqlite_transaction_guard(const sqlite_transaction_guard&) = delete;
    sqlite_transaction_guard(sqlite_transaction_guard&&) = delete;
    void operator=(const sqlite_transaction_guard&) = delete;
    void operator=(sqlite_transaction_guard&&) = delete;

    ~sqlite_transaction_guard() {
        if (commited) {
            return;
        }
        sqlite3_exec(db, "ROLLBACK", nullptr, nullptr, nullptr);
    }

    auto commit() noexcept {
        auto result = sqlite3_exec(db, "COMMIT", nullptr, nullptr, nullptr);
	commited = !result;
	return commited;
    }
};

void prepare_stmt(sqlite3 *db, sqlite_storage::stmt_ptr &target, const char *stmt_txt) {
    sqlite3_stmt *stmt;
    auto result = sqlite3_prepare_v2(db, stmt_txt, -1, &stmt, nullptr);
    if (result != SQLITE_OK) {
        sqlite3_close(db);
        throw std::runtime_error{sqlite3_errstr(result)};
    }
    target = sqlite_storage::stmt_ptr{stmt, sqlite3_finalize};
}
}  // namespace

boost::program_options::options_description sqlite_po::commandline_options() {
    namespace po = boost::program_options;
    boost::program_options::options_description descr{"SQLite Storage Options"};
    descr.add_options()("sqlite-file",
                        po::value<std::string>(&sqlite_file)->default_value("db.sqlite"),
                        "path to SQLite database file");
    return descr;
}

sqlite_storage sqlite_po::create_instance() const {
    return sqlite_storage{sqlite_file.c_str()};
}

sqlite_storage::card_stmts::card_stmts()
    : select_all{nullptr, &sqlite3_finalize},
      select_one{nullptr, &sqlite3_finalize},
      delete_one{nullptr, &sqlite3_finalize},
      insert{nullptr, &sqlite3_finalize} {
}

sqlite_storage::sqlite_storage(const char *f)
    : db{nullptr, &sqlite3_close} {
    sqlite3 *dbp;
    auto success = sqlite3_open(f, &dbp);
    db = std::unique_ptr<sqlite3, int (*)(sqlite3 *)>(dbp, &sqlite3_close);
    if (success != SQLITE_OK) {
        throw std::runtime_error{sqlite3_errstr(success)};
    }

    char *errmsg;
    const char *queries[]{
        "CREATE TABLE IF NOT EXISTS song_cards (card_id CHAR(10), card_name VARCHAR(255), "
        "song_title VARCHAR(255), song_ref VARCHAR(255), may_override BOOLEAN)",
        "CREATE TABLE IF NOT EXISTS playlist_cards (card_id CHAR(10), card_name VARCHAR(255), "
        "list_title VARCHAR(255), list_ref VARCHAR(255), may_override BOOLEAN)",
        "CREATE TABLE IF NOT EXISTS silence_cards (card_id CHAR(10), card_name VARCHAR(255), "
        "timeout INT)"

    };
    for (auto &&q : queries) {
        auto result = sqlite3_exec(dbp, q, nullptr, nullptr, &errmsg);

        if (result != SQLITE_OK) {
            std::string e{errmsg};
            sqlite3_free(errmsg);
            throw std::runtime_error{e};
        }
    }

    prepare_stmt(dbp,  stmts[card_type_song].select_all,
                 "SELECT rowid, card_id, card_name, song_title, song_ref, may_override FROM song_cards");
    prepare_stmt(dbp, stmts[card_type_song].select_one,
                 "SELECT rowid, card_id, card_name, song_title, song_ref, may_override FROM song_cards "
                 "WHERE card_id = ?");
    prepare_stmt(dbp, stmts[card_type_song].delete_one, "DELETE FROM song_cards WHERE card_id = ?");
    prepare_stmt(dbp, stmts[card_type_song].insert,
                 "INSERT INTO song_cards (card_id, card_name, song_title, song_ref, may_override) "
                 "VALUES (?,?,?,?,?)");

    prepare_stmt(dbp, stmts[card_type_playlist].select_all,
                 "SELECT rowid, card_id, card_name, list_title, list_ref, may_override FROM playlist_cards");
    prepare_stmt(dbp, stmts[card_type_playlist].select_one,
                 "SELECT rowid, card_id, card_name, list_title, list_ref, may_override FROM playlist_cards "
                 "WHERE card_id = ?");
    prepare_stmt(dbp, stmts[card_type_playlist].delete_one, "DELETE FROM playlist_cards WHERE card_id = ?");
    prepare_stmt(dbp, stmts[card_type_playlist].insert,
                 "INSERT INTO playlist_cards (card_id, card_name, list_title, list_ref, may_override) "
                 "VALUES (?,?,?,?,?)");

    prepare_stmt(dbp, stmts[card_type_silence].select_all,
                 "SELECT rowid, card_id, card_name, timeout FROM silence_cards");
    prepare_stmt(dbp, stmts[card_type_silence].select_one,
                 "SELECT rowid, card_id, card_name, timeout FROM silence_cards "
                 "WHERE card_id = ?");
    prepare_stmt(dbp, stmts[card_type_silence].delete_one, "DELETE FROM silence_cards WHERE card_id = ?");
    prepare_stmt(dbp, stmts[card_type_silence].insert,
                 "INSERT INTO silence_cards (card_id, card_name, timeout) "
                 "VALUES (?,?,?)");
}

namespace {
std::string column_to_string(sqlite3_stmt *stmt, int col) noexcept {
    return {reinterpret_cast<const char *>(sqlite3_column_text(stmt, col)),
            static_cast<unsigned long>(sqlite3_column_bytes(stmt, col))};
}

card_id_t column_to_card_id_t(sqlite3_stmt *stmt, int col) noexcept {
    auto tmp = column_to_string(stmt, col);
    return card_id_t(tmp.begin(), tmp.end());
}

template <typename T>
T row_to_card(sqlite3_stmt *) noexcept {
    T::row_to_card_unimplemented;
}

template <>
song_card row_to_card<song_card>(sqlite3_stmt *stmt) noexcept {
    song_card card{};

    card.id = sqlite3_column_int64(stmt, 0);
    card.card_id = column_to_card_id_t(stmt, 1);
    card.card_name = column_to_string(stmt, 2);
    card.song_title = column_to_string(stmt, 3);
    card.song_ref = column_to_string(stmt, 4);
    card.may_override = sqlite3_column_int(stmt, 5);

    return card;
}

template <>
playlist_card row_to_card<playlist_card>(sqlite3_stmt *stmt) noexcept {
    playlist_card card{};

    card.id = sqlite3_column_int64(stmt, 0);
    card.card_id = column_to_card_id_t(stmt, 1);
    card.card_name = column_to_string(stmt, 2);
    card.list_title = column_to_string(stmt, 3);
    card.list_ref = column_to_string(stmt, 4);
    card.may_override = sqlite3_column_int(stmt, 5);

    return card;
}

template <>
silence_card row_to_card<silence_card>(sqlite3_stmt *stmt) noexcept {
    silence_card card{};

    card.id = sqlite3_column_int64(stmt, 0);
    card.card_id = column_to_card_id_t(stmt, 1);
    card.card_name = column_to_string(stmt, 2);
    card.timeout = std::chrono::seconds{sqlite3_column_int(stmt, 3)};

    return card;
}
}  // namespace

std::vector<card_variant> sqlite_storage::get_all_cards() noexcept {
    std::vector<card_variant> retval;

    int i = 0;
    for (auto&& s : stmts) {
        auto stmt = s.select_all.get();
        while (sqlite3_step(stmt) == SQLITE_ROW) {
            switch (card_types(i)) {
                case card_type_song:
                    retval.emplace_back(row_to_card<song_card>(stmt));
                    break;
                case card_type_playlist:
                    retval.emplace_back(row_to_card<playlist_card>(stmt));
                    break;
                case card_type_silence:
                    retval.emplace_back(row_to_card<silence_card>(stmt));
                    break;
                default:
		    static_assert(std::tuple_size<decltype(stmts)>::value == 3);
            }
        }
        sqlite3_reset(stmt);
        i++;
    }

    return retval;
}

namespace {
template <typename T>
absl::optional<T> do_get_card(const sqlite_storage::card_stmts &stmts,
                              const std::vector<uint8_t> &id) {
    auto stmt = stmts.select_one.get();

    sqlite3_bind_blob(stmt, 1, id.data(), id.size(), nullptr);
    auto result = sqlite3_step(stmt);
    if (result != SQLITE_ROW) {
        sqlite3_reset(stmt);
        return {};
    }

    T card = row_to_card<T>(stmt);

    sqlite3_reset(stmt);
    return card;
}
}  // namespace

card_variant sqlite_storage::get_card(const std::vector<uint8_t> &id) noexcept {
    auto s_card = do_get_card<song_card>(stmts[card_type_song], id);
    if (s_card) {
        return s_card.value();
    }
    auto p_card = do_get_card<playlist_card>(stmts[card_type_playlist], id);
    if (p_card) {
        return p_card.value();
    }
    auto q_card = do_get_card<silence_card>(stmts[card_type_silence], id);
    if (q_card) {
        return q_card.value();
    }

    return empty_card{id};
}

namespace {
void bind_card(const song_card &card, sqlite3_stmt *stmt) {
    sqlite3_bind_blob(stmt, 1, card.card_id.data(), card.card_id.size(), nullptr);
    sqlite3_bind_text(stmt, 2, card.card_name.c_str(), card.card_name.length(), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 3, card.song_title.c_str(), card.song_title.length(), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 4, card.song_ref.c_str(), card.song_ref.length(), SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 5, card.may_override);
}

void bind_card(const playlist_card& card, sqlite3_stmt *stmt) {
    sqlite3_bind_blob(stmt, 1, card.card_id.data(), card.card_id.size(), nullptr);
    sqlite3_bind_text(stmt, 2, card.card_name.c_str(), card.card_name.length(), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 3, card.list_title.c_str(), card.list_title.length(), SQLITE_TRANSIENT);
    sqlite3_bind_text(stmt, 4, card.list_ref.c_str(), card.list_ref.length(), SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 5, card.may_override);
}

void bind_card(const silence_card& card, sqlite3_stmt *stmt) {
    sqlite3_bind_blob(stmt, 1, card.card_id.data(), card.card_id.size(), nullptr);
    sqlite3_bind_text(stmt, 2, card.card_name.c_str(), card.card_name.length(), SQLITE_TRANSIENT);
    sqlite3_bind_int(stmt, 3, card.timeout.count());
}

template <typename T>
bool do_store_card(const T &card, sqlite3 *db, const sqlite_storage::card_stmts_t &stmts,
                   size_t type_id) {
    auto stmt = stmts[type_id].insert.get();
    sqlite_transaction_guard transaction{db};

    for (auto &&s : stmts) {
        sqlite3_bind_blob(s.delete_one.get(), 1, card.card_id.data(), card.card_id.size(), nullptr);
        auto result = sqlite3_step(s.delete_one.get());
        sqlite3_reset(s.delete_one.get());
        if (result != SQLITE_DONE) {
            return false;
        }
    }

    bind_card(card, stmt);

    auto result = sqlite3_step(stmt);
    sqlite3_reset(stmt);

    if (result != SQLITE_DONE) {
        return false;
    }

    return transaction.commit();
}
}  // namespace

bool sqlite_storage::store(const song_card &card) noexcept {
    return do_store_card(card, db.get(), stmts, card_type_song);
}

bool sqlite_storage::store(const playlist_card &card) noexcept {
    return do_store_card(card, db.get(), stmts, card_type_playlist);
}

bool sqlite_storage::store(const silence_card &card) noexcept {
    return do_store_card(card, db.get(), stmts, card_type_silence);
}

namespace {
struct card_store_visitor {
    sqlite_storage& store;

    bool operator()(const empty_card &) const {
        return true;
    }
    template <typename card_t>
    bool operator()(const card_t& card) const {
        return store.store(card);
    }
};
}  // namespace


bool sqlite_storage::store(const card_variant &card) noexcept {
    card_id_t id{card_id(card)};
    {
        sqlite_transaction_guard transaction{db.get()};
        for (auto &&s : stmts) {
	    auto deleter = s.delete_one.get();
            sqlite3_bind_blob(deleter, 1, id.data(), id.size(), nullptr);
            auto result = sqlite3_step(deleter);
            sqlite3_reset(deleter);
            if (result != SQLITE_DONE) {
                return false;
            }
        }
	transaction.commit();
    }
    return absl::visit(card_store_visitor{*this}, card);
}

}  // namespace musicbox::storage
