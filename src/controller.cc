#include "controller.h"

#include <cassert>
#include <cstdio>
#include <iostream>
#include <memory>

#include "http.h"
#include "musicbox_config.h"

#include <nlohmann/json.hpp>
#include <absl/strings/string_view.h>

namespace musicbox {
namespace {
using namespace std::string_literals;
using json = nlohmann::json;
using string_view = absl::string_view;
using websocket_session = http::websocket_session;

void close_file(std::FILE* fp) { std::fclose(fp); }
using file = std::unique_ptr<std::FILE, decltype(&close_file)>;

void report_error(websocket_session *websocket, string_view reason,
                         string_view detail = {}) {
    json error_data{{"action", "error"}, {"reason", reason}};
    if (detail.data()) {
        error_data["detail"] = detail;
    }
    websocket->send(error_data.dump());
}

void handle_connect(websocket_session *websocket, controller *ctrl) {
    auto d = json{};
    d["action"] = "connect";
    auto version = json{};
    version["git_branch"] = musicbox::GIT_BRANCH;
    version["git_commit"] = musicbox::GIT_COMMIT_HASH;
    d["version"] = version;

    file fp{popen("/usr/bin/uptime", "r"), close_file};
    if (fp) {
        char uptime[256];
        auto size = std::fread(uptime, sizeof(char), 255, fp.get());
        uptime[size] = '\0';
        d["uptime"] = uptime;
    }

    websocket->send(d.dump());

    json all_cards_data{{"action", "all_cards"}, {"cards", ctrl->get_all_cards()}};
    websocket->send(all_cards_data.dump());
}

void handle_store(websocket_session *websocket, controller *ctrl, const json &d) try {
    card_variant card = d.at("card").get<card_variant>();
    ctrl->store(card);
} catch (const std::exception &e) {
    report_error(websocket, "Failed to store card", e.what());
}
}  // namespace

void controller::configuration::receive_command(absl::string_view data) {
    auto websocket = this->websocket.lock();
    assert(!!websocket);
    std::string action;

    json d;

    try {
        d = json::parse(data);
        d["action"].get_to(action);
    } catch (const nlohmann::json::exception &e) {
        report_error(websocket.get(), "Invalid message received", e.what());
        return;
    }

    if (action == "connect"s) {
        handle_connect(websocket.get(), ctrl);
    } else if (action == "store") {
        handle_store(websocket.get(), ctrl, d);
    } else {
        report_error(websocket.get(), "Called unknown action '"s + action + "'");
    }
}

void controller::configuration::send_card(const card_variant &card) {
    auto websocket = this->websocket.lock();
    assert(!!websocket);

    json data{{"action", "card_touched"}, {"card", card}};
    websocket->send(data.dump());
}

void controller::configuration::connect(std::shared_ptr<websocket_session> websocket) {
    this->websocket = websocket;
}
}  // namespace musicbox
