#include "controller_mocks.h"

using namespace musicbox::test;

TEST_F(controller_tests, instatiate) {
    EXPECT_TRUE(controller.is_idle());
}

TEST_F(controller_tests, empty_card_does_nothing) {
    EXPECT_CALL(storage, get_card(testing::Eq(empty.card_id)));

    controller.card_approached(empty.card_id);
    EXPECT_TRUE(controller.is_idle());
}

TEST_F(controller_tests, song_card_starts_playing) {
    EXPECT_CALL(storage, get_card(testing::_)).Times(testing::AnyNumber());
    EXPECT_CALL(player, play(testing::Eq("song.mp3"))).WillOnce(testing::Return(true));

    controller.card_approached(song.card_id);

    // TODO(johannes): In a later stage we want a short timer to prevent restarting the same song
    // all the time by going to a temporary state and only after few seconds to idle
    EXPECT_TRUE(controller.is_idle());
}

TEST_F(controller_tests, playlist_card_starts_playing) {
    EXPECT_CALL(storage, get_card(testing::_)).Times(testing::AnyNumber());
    EXPECT_CALL(player, play_list(testing::Eq("list.m3u"))).WillOnce(testing::Return(true));

    controller.card_approached(playlist.card_id);

    // TODO(johannes): In a later stage we want a short timer to prevent restarting the same song
    // all the time by going to a temporary state and only after few seconds to idle
    EXPECT_TRUE(controller.is_idle());
}
