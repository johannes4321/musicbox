#include "controller_mocks.h"

using namespace musicbox::test;

TEST_F(controller_tests, switch_to_config_mode) {
    {
        auto config = controller.start_config();
        EXPECT_TRUE(controller.is_in_config());
        EXPECT_FALSE(controller.is_idle());
    }
    EXPECT_FALSE(controller.is_in_config());
    EXPECT_TRUE(controller.is_idle());
}

TEST_P(controller_config_error_tests, respond_to_error) {
    std::string command{GetParam()};

    EXPECT_CALL(*session.get(),
                send(testing::AllOf(JsonKeyEq("action", "error"),
                                    JsonKeyEq("reason", "Invalid message received"))))
        .Times(1);

    config->receive_command(command);
}

INSTANTIATE_TEST_SUITE_P(controller_config_p, controller_config_error_tests,
                         ::testing::Values(R"({"foo":"bar"})",
                                           R"(this is no valid json, I am sorry)",
                                           R"(["foo","bar"])"));

TEST_F(controller_config_tests, respond_to_invalid_action) {
    std::string command{R"({"action":"invalid action, which does not exist"})"};

    EXPECT_CALL(
        *session.get(),
        send(testing::AllOf(
            JsonKeyEq("action", "error"),
            JsonKeyEq("reason", "Called unknown action 'invalid action, which does not exist'"))))
        .Times(1);

    config->receive_command(command);
}

TEST_F(controller_config_tests, respond_to_config_connect) {
    std::string command{R"({"action":"connect"})"};
    EXPECT_CALL(storage, get_all_cards()).Times(testing::AnyNumber());

    EXPECT_CALL(*session.get(), send(JsonKeyEq("action", "all_cards"))).Times(1);
    EXPECT_CALL(*session.get(), send(JsonKeyEq("action", "connect"))).Times(1);

    config->receive_command(command);
}

TEST_F(controller_config_tests, config_connect_contains_empty_card_set_by_default) {
    std::string command{R"({"action":"connect"})"};

    EXPECT_CALL(*session.get(),
                send(testing::AllOf(JsonKeyEq("action", "all_cards"),
                                    JsonKeyEq("cards", std::vector<musicbox::card_variant>{}))));

    EXPECT_CALL(storage, get_all_cards()).Times(testing::AnyNumber());
    EXPECT_CALL(*session.get(), send(JsonKeyEq("action", "connect"))).Times(testing::AnyNumber());

    config->receive_command(command);
}

TEST_F(controller_config_tests, config_connect_contains_cards_from_storage) {
    std::string command{R"({"action":"connect"})"};

    std::vector<musicbox::card_variant> cards{song, playlist};

    EXPECT_CALL(*session.get(),
                send(testing::AllOf(JsonKeyEq("action", "all_cards"),
                                    JsonKeyEq("cards", cards))));

    EXPECT_CALL(storage, get_all_cards()).WillOnce(testing::Return(cards));

    EXPECT_CALL(*session.get(), send(JsonKeyEq("action", "connect"))).Times(testing::AnyNumber());

    config->receive_command(command);
}

TEST_F(controller_config_tests, config_connect_contains_version_info) {
    std::string command{R"({"action":"connect"})"};
    nlohmann::json exp;
    exp["git_branch"] = musicbox::GIT_BRANCH;
    exp["git_commit"] = musicbox::GIT_COMMIT_HASH;

    EXPECT_CALL(*session.get(),
                send(testing::AllOf(JsonKeyEq("action", "connect"), JsonKeyEq("version", exp))));

    EXPECT_CALL(storage, get_all_cards()).Times(testing::AnyNumber());
    EXPECT_CALL(*session.get(), send(JsonKeyEq("action", "all_cards"))).Times(testing::AnyNumber());

    config->receive_command(command);
}

TEST_F(controller_config_tests, report_card_touched_while_in_config) {
    EXPECT_CALL(storage, get_card(testing::_)).Times(testing::AnyNumber());

    EXPECT_CALL(*session.get(), send(testing::AllOf(JsonKeyEq("action", "card_touched"),
                                                    JsonKeyEq("card", playlist))));

    controller.card_approached(playlist.card_id);
}

TEST_F(controller_config_tests, report_error_on_invalid_card) {
    std::string none{R"({"action":"store"})"};
    std::string invalid_type{R"({"action":"store", "card":{ "type": "invalid type"}})"};
    std::string invalid_card{R"({"action":"store", "card":{ "type": "song", "missing":"proprties"}})"};

    EXPECT_CALL(*session.get(), send(testing::AllOf(JsonKeyEq("action", "error"),
                                                    JsonKeyEq("reason", "Failed to store card"))))
        .Times(3);

    config->receive_command(none);
    config->receive_command(invalid_type);
    config->receive_command(invalid_card);
}

TEST_F(controller_config_tests, store_empty_card)
{
    nlohmann::json command;
    command["action"] = "store";
    command["card"] = empty;
    auto cmd = command.dump();

    EXPECT_CALL(storage, store(testing::_));

    config->receive_command(cmd);
}
