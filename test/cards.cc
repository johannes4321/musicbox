#include "cards.h"

#include "nlohmann/json.hpp"
#include "gtest/gtest.h"

using namespace musicbox;
using nlohmann::json;

TEST(cards, empty_card_json_encode) {
  empty_card empty{{'a', 'b', 'c' } };
  json empty_js = empty;

  EXPECT_EQ(empty_js, "{\"type\":\"empty\", \"card_id\":[97,98,99]}"_json);
}

TEST(cards, empty_card_json_decode) {
  nlohmann::json json;
  json["type"] = "empty";
  json["card_id"] = std::vector<int>{1, 2, 3, 4};

  empty_card exp{{1, 2, 3, 4}};

  empty_card card = json.get<empty_card>();
  EXPECT_EQ(card, exp);
}

TEST(cards, song_card_json_encode) {
    song_card empty{1, {'a', 'b', 'c'}, "test card", "some song", "some_song.mp3", true};
    json empty_js = empty;

    EXPECT_EQ(empty_js, R"({
     "type":         "song",
     "id":           1,
     "card_id":      [97,98,99],
     "card_name":    "test card",
     "song_title":   "some song",
     "song_ref":     "some_song.mp3",
     "may_override": true 
     })"_json);
}

TEST(cards, song_card_json_decode) {
    nlohmann::json json;
    json["type"] = "song";
    json["card_id"] = std::vector<int>{1, 2, 3, 4};
    json["id"] = 1;
    json["card_name"] = "test card";
    json["song_title"] = "some song";
    json["song_ref"] = "some_song.mp3";
    json["may_override"] = true;

    song_card exp{1, {1, 2, 3, 4}, "test card", "some song", "some_song.mp3", true};

    song_card card = json.get<song_card>();
    EXPECT_EQ(card, exp);
}

TEST(cards, playlist_card_json_encode) {
    playlist_card card{1, {'a', 'b', 'c'}, "test card", "some playlist", "playlist.m3u", true};
    json card_js = card;

    EXPECT_EQ(card_js, R"({
     "type":         "playlist",
     "id":           1,
     "card_id":      [97,98,99],
     "card_name":    "test card",
     "list_title":   "some playlist",
     "list_ref":     "playlist.m3u",
     "may_override": true 
     })"_json);
}

TEST(cards, playlist_card_json_decode) {
    nlohmann::json json;
    json["type"] = "playlist";
    json["card_id"] = std::vector<int>{1, 2, 3, 4};
    json["id"] = 1;
    json["card_name"] = "list card";
    json["list_title"] = "some list";
    json["list_ref"] = "some_list.m3u";
    json["may_override"] = true;

    playlist_card exp{1, {1, 2, 3, 4}, "list card", "some list", "some_list.m3u", true};

    playlist_card card = json.get<playlist_card>();
    EXPECT_EQ(card, exp);
}

TEST(cards, silence_card_json_encode) {
    silence_card empty{1, {'a', 'b', 'c'}, "silence, please", std::chrono::seconds{10}};
    json empty_js = empty;

    EXPECT_EQ(empty_js, R"({
     "type":         "silence",
     "id":           1,
     "card_id":      [97,98,99],
     "card_name":    "silence, please",
     "timeout":      10
     })"_json);

    empty.timeout = std::chrono::minutes{2};
    empty_js = empty;
    EXPECT_EQ(empty_js, R"({
      "type":         "silence",
      "id":           1,
      "card_id":      [97,98,99],
      "card_name":    "silence, please",
      "timeout":      120
    })"_json);
}

TEST(cards, silence_card_json_decode) {
    nlohmann::json json;
    json["type"] = "silence";
    json["card_id"] = std::vector<int>{1, 2, 3, 4};
    json["id"] = 1;
    json["card_name"] = "silence card";
    json["timeout"] = 10;

    silence_card exp{1, {1, 2, 3, 4}, "silence card", std::chrono::seconds{10}};

    silence_card card = json.get<silence_card>();
    EXPECT_EQ(card, exp);
}

TEST(cards, card_variant_json_encode) {
    card_variant empty{empty_card{{'a', 'b', 'c'}}};
    json empty_js = empty;

    EXPECT_EQ(empty_js, "{\"type\":\"empty\", \"card_id\":[97,98,99]}"_json);
}

TEST(cards, card_variant_json_decode) {
    card_id_t exp{'a', 'b', 'c'};

    empty_card empty{{'a', 'b', 'c'}};
    song_card song{1, {'a', 'b', 'c'}, "test card", "some song", "some_song.mp3", true};
    playlist_card list{1, {'a', 'b', 'c'}, "test card", "some playlist", "playlist.m3u", true};
    silence_card silence{1, {'a', 'b', 'c'}, "silence, please", std::chrono::seconds{10}};

    json tmp;

    tmp = empty;
    EXPECT_EQ(exp, card_id(tmp.get<card_variant>()));

    tmp = song;
    EXPECT_EQ(exp, card_id(tmp.get<card_variant>()));

    tmp = list;
    EXPECT_EQ(exp, card_id(tmp.get<card_variant>()));

    tmp = silence;
    EXPECT_EQ(exp, card_id(tmp.get<card_variant>()));
}

TEST(cards, verify_send_to_stream) {
    // this doesn't test the output format, just checks there is no error
    std::vector<card_variant> cards{
        empty_card{{'a', 'b', 'c'}},
        song_card{1, {'a', 'b', 'c'}, "test card", "some song", "some_song.mp3", true},
        playlist_card{1, {'a', 'b', 'c'}, "test card", "some playlist", "playlist.m3u", true},
        silence_card{1, {'a', 'b', 'c'}, "silence, please", std::chrono::seconds{10}}};

    for (card_variant c : cards) {
        std::stringstream ss;
        ss << c;
        EXPECT_TRUE(ss.good());
    }
}

TEST(cards, differen_types_cant_be_same) {
    card_variant empty = empty_card{{'a', 'b', 'c'}};
    card_variant song =
        song_card{1, {'a', 'b', 'c'}, "test card", "some song", "some_song.mp3", true};
    card_variant list =
        playlist_card{1, {'a', 'b', 'c'}, "test card", "some playlist", "playlist.m3u", true};
    card_variant silence =
        silence_card{1, {'a', 'b', 'c'}, "silence, please", std::chrono::seconds{10}};

    EXPECT_NE(empty, song);
    EXPECT_NE(empty, list);
    EXPECT_NE(empty, silence);

    EXPECT_NE(song, list);
    EXPECT_NE(song, silence);

    EXPECT_NE(list, silence);
}
