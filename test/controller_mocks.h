#pragma once

#include "main_controller.h"

#include "http.h"
#include "musicbox_config.h"

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <absl/strings/string_view.h>
#include <nlohmann/json.hpp>

namespace musicbox::test {
class mock_player {
   public:
    MOCK_CONST_METHOD1(play, bool(absl::string_view songid));
    MOCK_CONST_METHOD1(play_list, bool(absl::string_view listid));
};

class mock_storage {
   public:
    MOCK_CONST_METHOD0(get_all_cards, std::vector<musicbox::card_variant>());
    MOCK_CONST_METHOD1(get_card, musicbox::card_variant(const musicbox::card_id_t &id));
    MOCK_METHOD1(store, bool(const musicbox::card_variant &card));
};

class mock_http_websocket_session : public musicbox::http::websocket_session {
   public:
    MOCK_METHOD1(send, void(const std::string& data));
};

class controller_tests : public testing::Test {
   protected:
    musicbox::null_logger logger{};
    mock_player player;
    mock_storage storage;

    const musicbox::empty_card empty;
    const musicbox::song_card song;
    const musicbox::playlist_card playlist;

    musicbox::main_controller<mock_player, mock_storage, musicbox::null_logger> controller;

    explicit controller_tests()
        : logger{},
          player{},
          storage{},
          empty{{'e', 'm', 'p', 't', 'y'}},
          song{2, {'s', 'o', 'n', 'g'}, "song card", "song title", "song.mp3", false},
          playlist{3, {'l', 'i', 's', 't'}, "playlist card", "list", "list.m3u", false},
          controller{player, storage, logger} {
        using ::testing::Eq, ::testing::Return;

        ON_CALL(storage, get_card(Eq(empty.card_id))).WillByDefault(Return(empty));
        ON_CALL(storage, get_card(Eq(song.card_id))).WillByDefault(Return(song));
        ON_CALL(storage, get_card(Eq(playlist.card_id))).WillByDefault(Return(playlist));
    }
};

class controller_config_tests : public controller_tests {
   protected:
    std::shared_ptr<mock_http_websocket_session> session;
    musicbox::controller::configuration::ptr config;

    explicit controller_config_tests()
        : controller_tests{},
          session{std::make_shared<mock_http_websocket_session>()},
          config{controller.start_config()} {
        config->connect(session);
    }
};

class controller_config_error_tests : public controller_config_tests,
                                      public ::testing::WithParamInterface<const char *> {};

class JsonKeyMatcher : public testing::MatcherInterface<const std::string&> {
    std::string key;
    nlohmann::json expected;

   public:
    explicit JsonKeyMatcher(const std::string& key, const nlohmann::json& expected)
        : key{key}, expected{expected} {
    }

    virtual bool MatchAndExplain(const std::string& value,
                                 testing::MatchResultListener* listener) const;
    virtual void DescribeTo(::std::ostream* os) const;
    virtual void DescribeNegationTo(::std::ostream* os) const;
};

inline testing::Matcher<const std::string&> JsonKeyEq(const std::string& key,
                                                      const nlohmann::json& expected) {
    return MakeMatcher(new JsonKeyMatcher(key, expected));
}

}  // namespace

