add_executable(all_tests test_main.cc cards.cc controller.cc controller_config.cc controller_mocks.cc storage/test_sqlite_storage.cc)
target_include_directories(all_tests PRIVATE "${CMAKE_CURRENT_SOURCE_DIR}/../src")
target_include_directories(all_tests PRIVATE SYSTEM ${Boost_INCLUDE_DIRS} "${CMAKE_CURRENT_SOURCE_DIR}/../lib/sml/include")

target_link_libraries(all_tests gtest gmock storage nlohmann_json::nlohmann_json absl::variant controller)

find_package(GTest)
gtest_discover_tests(all_tests)
