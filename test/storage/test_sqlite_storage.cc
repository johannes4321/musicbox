#include "storage/sqlite_storage.h"

#include "gtest/gtest.h"

#include <unistd.h>
#include <sys/stat.h>

using namespace musicbox;
using namespace musicbox::storage;

sqlite_storage memory_storage(const testing::TestInfo* info) {
    auto keep_db = !!getenv("MUSICBOX_SQLITE_KEEP_DB");

    if (!keep_db) {
        return sqlite_storage{":memory:"};
    }

    auto path = std::string{"testresult-"} + std::to_string(getpid());
    mkdir(path.c_str(), 0777);

    auto file = std::string{path} + "/" + info->name() + ".sqlite3";
    return sqlite_storage{file.c_str()};
}

TEST(sqlite_storage, instantiate_storage) {
    ASSERT_NO_THROW(sqlite_storage storage{":memory:"});
}

TEST(sqlite_storage, throw_on_invald_file_name) {
    ASSERT_THROW(
        sqlite_storage storage{
            "/this/diretory/hopefully/does/not/exist/db.sqlite"},
        std::runtime_error);
}

TEST(sqlite_storage, can_store_song_card) {
    auto storage{memory_storage(test_info_)};
    song_card card{1, {1, 2, 3, 4}, "Test Card", "Test Title", "test_ref.mp3", 1};
    EXPECT_TRUE(storage.store(card));
}

TEST(sqlite_storage, gives_empty_card) {
    auto storage{memory_storage(test_info_)};

    EXPECT_NO_THROW(absl::get<empty_card>(storage.get_card({1,2,3,4})));
    song_card card{1, {1, 2, 3, 4}, "Test Card", "Test Title", "test_ref.mp3", 1};
    storage.store(card);
    EXPECT_NO_THROW(absl::get<empty_card>(storage.get_card({2,3,4,5})));
}

TEST(sqlite_storage, can_retrieve_song_card) {
    auto storage{memory_storage(test_info_)};

    song_card card{1, {1, 2, 3, 4}, "Test Card", "Test Title", "test_ref.mp3", 1};
    EXPECT_TRUE(storage.store(card));

    auto result = storage.get_card({1,2,3,4});
    EXPECT_EQ(1, result.index());
    auto result_card = absl::get<song_card>(result);
    EXPECT_EQ(card, result_card);
}

TEST(sqlite_storage, can_retrieve_playlist_card) {
    auto storage{memory_storage(test_info_)};

    playlist_card card{1, {1, 2, 3, 4}, "Test Card", "Test Title", "test_ref.m3u", 1};
    EXPECT_TRUE(storage.store(card));

    auto result = storage.get_card({1,2,3,4});
    EXPECT_EQ(2, result.index());
    auto result_card = absl::get<playlist_card>(result);
    EXPECT_EQ(card, result_card);
}

TEST(sqlite_storage, can_retrieve_silence_card) {
    auto storage{memory_storage(test_info_)};

    silence_card card{1, {1, 2, 3, 4}, "Silence, plase", std::chrono::seconds{10}};
    EXPECT_TRUE(storage.store(card));

    auto result = storage.get_card({1,2,3,4});
    EXPECT_EQ(3, result.index());
    auto result_card = absl::get<silence_card>(result);
    EXPECT_EQ(card, result_card);
}


TEST(sqlite_storage, stored_card_should_persist) {
    std::unique_ptr<const char, decltype(&::unlink)> filename{"test_db.sqlite", ::unlink};
    song_card card{1, {1, 2, 3, 4}, "Test Card", "Test Title", "test_ref.mp3", 1};

    {
        sqlite_storage storage{filename.get()};
        EXPECT_TRUE(storage.store(card));
    }
    {
        sqlite_storage storage{filename.get()};
        auto result_card = absl::get<song_card>(storage.get_card({1, 2, 3, 4}));
        EXPECT_EQ(card, result_card);
    }
}


TEST(sqlite_storage, overwriting_should_have_effect) {
    auto storage{memory_storage(test_info_)};

    song_card card1{1, {1, 2, 3, 4}, "Test Card", "Test Title", "test_ref.mp3", 1};
    EXPECT_TRUE(storage.store(card1));

    auto result1_card = absl::get<song_card>(storage.get_card({1,2,3,4}));
    EXPECT_EQ(card1, result1_card);

    playlist_card card2{1, {1, 2, 3, 4}, "Other Card", "Other Title", "other_ref.m3u", 0};
    EXPECT_TRUE(storage.store(card2));

    auto result2_card = absl::get<playlist_card>(storage.get_card({1,2,3,4}));
    EXPECT_EQ(card2, result2_card);

    silence_card card3{1, {1, 2, 3, 4}, "Other Card", std::chrono::minutes{5}};
    EXPECT_TRUE(storage.store(card3));

    auto result3_card = absl::get<silence_card>(storage.get_card({1,2,3,4}));
    EXPECT_EQ(card3, result3_card);
}

TEST(sqlite_storage, get_all_cards_empty) {
    auto storage{memory_storage(test_info_)};
    std::vector<card_variant> expected{};
    auto result = storage.get_all_cards();
    EXPECT_EQ(expected, result);
}

TEST(sqlite_storage, get_all_cards) {
    auto storage{memory_storage(test_info_)};

    std::vector<card_variant> expected{
        song_card{1, {1, 2, 3, 4}, "Test Card", "Test Title", "test_ref.mp3", 1},
        song_card{2, {5, 6, 7, 8}, "Other Card", "Other Title", "other_ref.mp3", 0},
        playlist_card{1, {2, 3, 4, 5}, "List Card", "My cool list", "list.m3u", 0},
	silence_card{1, {9, 8, 7, 6}, "Silence", std::chrono::minutes{3}},
    };
    storage.store(absl::get<song_card>(expected[0]));
    storage.store(absl::get<song_card>(expected[1]));
    storage.store(absl::get<playlist_card>(expected[2]));
    storage.store(absl::get<silence_card>(expected[3]));

    auto result = storage.get_all_cards();
    EXPECT_EQ(expected, result);
}

TEST(sqlite_storage, can_store_variant_card) {
    auto storage{memory_storage(test_info_)};

    card_id_t id{1,2,3,4};
    song_card song{1, {1, 2, 3, 4}, "Test Card", "Test Title", "test_ref.mp3", 1};
    empty_card empty{{1, 2, 3, 4}};

    EXPECT_TRUE(storage.store(card_variant{song}));
    EXPECT_EQ(song, storage.get_card(id));
    EXPECT_TRUE(storage.store(card_variant{empty}));
    EXPECT_EQ(empty, storage.get_card(id));
}
