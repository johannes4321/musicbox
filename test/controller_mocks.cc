#include "controller_mocks.h"

namespace musicbox::test {
bool JsonKeyMatcher::MatchAndExplain(const std::string& value,
                                     testing::MatchResultListener*) const {
    auto json = nlohmann::json::parse(value);
    return json[key] == expected;
}
void JsonKeyMatcher::DescribeTo(::std::ostream* os) const {
    *os << "key '" << key << "' equals '" << expected << "'";
}
void JsonKeyMatcher::DescribeNegationTo(::std::ostream* os) const {
    *os << "key '" << key << "' does not equal '" << expected << "'";
}

}  // namespace musicbox::test
