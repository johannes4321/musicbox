musicbox
========

This project aims to allow my nephew to pick his music by using RFID tokens.

Hardware goals
--------------

This software is supposed to run on an Raspberry Pi Zero W and an RC522 RFID module. Development is done on Linux x86-64.

Music backends
--------------

Productive music backend is a SONOS setup. For dvelopment I however also support mpd (music player daemon)

State
-----

This is completely experimental.

Architecture
------------

Key component is a C++ daemon watching the RFID module and controlling the music backend. Also contains an webserver for providing a more or less simple UI to assign tracks to RFID tokens.

Building
--------

For building you need a recent C++ compiler (C++17) and cmake as well as the libraries refereced as submdule in `lib/`.

With some luck a build can suceed like this:

    $ mkdir build
    $ cd build
    $ cmake ..
    $ make

For building the frontend set `-DNODE_EXE` and `-DNPM_EXE` to a Node.js runtime and npm or have them in path for CMake and then run

    $ make frontend

After that `src/musicbox` is the main daemon. `test/all_tests` is a test runner and `src/utils` contains a random collection of different utilities for direct use of different subsystems. To sart the MusicBox run

    $ ./src/musicbox

And open http://localhost:8008/

