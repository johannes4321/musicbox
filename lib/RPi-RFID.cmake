add_library(RPi-RFID STATIC RPi-RFID/MFRC522.cpp)
target_link_libraries(RPi-RFID PRIVATE bcm2835)
target_include_directories(RPi-RFID PUBLIC RPi-RFID)

