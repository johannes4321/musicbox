file(GLOB MPD_SRCS libmpdclient/src/*.c)

add_library(mpdclient STATIC ${MPD_SRCS})

target_include_directories(mpdclient PUBLIC libmpdclient/include)
target_include_directories(mpdclient PRIVATE libmpdclient/src)
target_include_directories(mpdclient PUBLIC libmpdclient_config)


