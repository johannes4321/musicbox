
const WebSocket = window.WebSocket || window.MozWebSocket;


export default {
    connect(handler) {
        const connection = new WebSocket('ws://' + 'localhost:8008');// window.location.host);


        connection.onopen = function () {
            const data = JSON.stringify({action: 'connect', name});
            connection.send(data);
            handler.connect();
        };

        connection.onerror = function (error) {
            alert(JSON.stringify(error));
        };

        connection.onclose = function () {
            console.log('closed');
            handler.disconnect();
        };

        connection.onmessage = function (message) {
            const json = JSON.parse(message.data);
            console.log(message);
            handler[json.action](json);
        };
    }
}