"use strict";

import { expect } from 'chai'
import { shallowMount } from '@vue/test-utils'
import HelloWorld from '@/components/HelloWorld.vue'
import MusicboxCard from "../../src/components/MusicboxCard";

describe('HelloWorld.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    })
    expect(wrapper.text()).to.include(msg)
  })
})

describe('MusicboxCard', () => {
  it('formats the card\'s id', () => {
    const card = {
      type: "empty",
      card_id: [0, 10, 15, 16, 255]
    };
    const wrapper = shallowMount(MusicboxCard, {
      propsData: {card}
    });
    expect(wrapper.text()).to.include("00:0a:0f:10:ff");
  });
});